import cv2
import numpy as np
import matplotlib.pyplot as plt

import drone_util.image_util as image_util

from config import *

#initialize color map
cmap = plt.get_cmap('tab20b')
colors = [cmap(i)[:3] for i in np.linspace(0, 1, 20)]

class SideViewHandler:
    @staticmethod
    def side_frame_valid(frame_index):
        if frame_index % SIDEVIEW_NUM == 0:
            return True
        else:
            return False

    def __init__(self):
        self.sideview_list = []


    def display_sideview(self, frame, crop_img = None):
        MAX_COUNT = 10
        IMG_W, IMG_H = 100, 100 # crop_img 리사이즈 크기
        frame_height, frame_width, _ = frame.shape
        #print(frame_height, frame_width)

        if image_util.is_valid(crop_img):
            crop_img = image_util.resize(crop_img, IMG_W, IMG_H)
            self.sideview_list.append(crop_img)
            if len(self.sideview_list) > MAX_COUNT:
                self.sideview_list.pop(0)

        for index, img_box in enumerate(self.sideview_list):
            y1_offset = 50
            x1 = 50
            x2 = x1 + IMG_W
            # if index == 0:
            #     y1 = frame_height - IMG_H * (index + 1)
            #     y2 = frame_height - IMG_H * (index)
            # else:
            #     y1 = y2 - IMG_H
            
            # y1 = y1_offset + frame_height - IMG_H * (index + 1)
            y1 = y1_offset + IMG_H * index
            y2 = y1 + IMG_H
            #print(x1, y1, x2, y2)
            # image.paste(box_region, (0, y1, box_region.width, y2))
            #print(img_box.shape)
            frame[y1:y2, x1:x2] = img_box       