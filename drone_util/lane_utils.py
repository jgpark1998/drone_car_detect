import numpy as np
import matplotlib.pyplot as plt
import cv2

def change_coord(points, height):
    new_points = []
    for p in points:
        x, y = p
        new_points.append([x, height-y])

    return new_points


def get_x_intercept(point1, point2):
    x1,y1 = point1
    x2,y2 = point2
    x_inter = (-1)*y1 * ((x2-x1)/(y2-y1)) + x1
    return x_inter


def get_x_intercept_in_roi(point1, point2, roi, h):
    start_x, start_y, end_x, end_y = roi
    converted_y = h - end_y ## coord 변환
    x1,y1 = point1
    x2,y2 = point2
    if x2-x1 == 0 or y2-y1 == 0:
        return None
    else:
        roi_x_inter = (converted_y + (-1)*y1) * ((x2-x1)/(y2-y1)) + x1
        return roi_x_inter


def get_angle(vec):
    a = [1,0]
    b = vec
    unit_vector_1 = a / np.linalg.norm(a)
    unit_vector_2 = b / np.linalg.norm(b)
    dot_product = np.dot(unit_vector_1, unit_vector_2)
    angle = np.arccos(dot_product)
    angle = angle/ np.pi * 180
    c = np.cross(b,a)
    if c>0:
        angle +=180
    return angle

def calc_dist_point_line(point, line_p1, line_p2):
    p_x, p_y = point
    line_x1, line_y1 = line_p1
    line_x2, line_y2 = line_p2
    area = abs((line_x1-p_x) * (line_y2-p_y) - (line_y1-p_y) * (line_x2-p_x))
    AB = ((line_x1-line_x2)**2 + (line_y1-line_y2)**2)**0.5
    distance = area//AB
    return distance

def remove_outliers(data, key):
    x_inter_only = [d[key] for d in data]
    u = np.mean(x_inter_only)
    s = np.std(x_inter_only)
    data_filtered = [d for d in data if (d[key] > u-2*s) & (d[key] < u+2*s)]
    return data_filtered


def is_box_in_roi(roi_box, crop_box):
    if crop_box["x"] < roi_box[0] or \
    crop_box["y"] < roi_box[1] or \
    (crop_box["w"] + crop_box["x"]) > (roi_box[2]) or \
    (crop_box["h"] + crop_box["y"]) > (roi_box[3]):
        return False
    else:
        return True

def draw_dots(img, forward_center_groups, backward_centers):
    cmap = plt.get_cmap('tab20b')
    for i, center_group in enumerate(forward_center_groups):
        dot_colors = [cmap(i)[:3] for i in np.linspace(0, 5, 20)]
        dot_color = dot_colors[i % len(dot_colors)]
        dot_color = [i * 255 for i in dot_color]

        for center in center_group:
            cv2.circle(img, (int(center[0]), int(center[1])), 10, dot_color, -1)

    for center in backward_centers:
        cv2.circle(img, (int(center[0]), int(center[1])), 10, (255,255,0), -1)


def draw_lane_lines(img, forward_lines, backward_lines, roi):
    roi_x1, roi_y1, roi_x2, roi_y2 = roi
    rect = (roi_x1, roi_y1, roi_x2-roi_x1, roi_y2-roi_y1) # to clip line
    h,w,_ = img.shape
    for line in forward_lines:
        bottom = (line['bottom'][0], h-line['bottom'][1])
        top = (line['top'][0], h-line['top'][1])
        ret, p1, p2 = cv2.clipLine(rect, bottom, top)
        cv2.line(img, (int(p1[0]), int(p1[1])), (int(p2[0]), int(p2[1])), (153,51,255), thickness=3)
        # cv2.line(img, (int(point1[0]), int(h-point1[1])), (int(point2[0]), int(h-point2[1])), (153,51,255), thickness=3)
    
    for line in backward_lines:
        bottom = (line['bottom'][0], h-line['bottom'][1])
        top = (line['top'][0], h-line['top'][1])
        ret, p1, p2 = cv2.clipLine(rect, bottom, top) 
        cv2.line(img, (int(p1[0]), int(p1[1])), (int(p2[0]), int(p2[1])), (0,255,255), thickness=3)
        # cv2.line(img, (int(point1[0]), int(h-point1[1])), (int(point2[0]), int(h-point2[1])), (0,255,255), thickness=3)
    