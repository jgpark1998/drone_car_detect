import os
# os.environ['CUDA_VISIBLE_DEVICES'] = '1'

import time

import shutil
import cv2
# import keras.backend as K
import numpy as np

import tensorflow as tf
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, Dense, GlobalAveragePooling2D
from tensorflow.keras.applications import EfficientNetB0
from tensorflow.keras.applications.efficientnet import preprocess_input
from tensorflow.keras.preprocessing import image

class EFNetHelper:


    def __init__(self, settings):
        self.settings = settings
        self.model = self._load_model()


    def _load_model(self):

        # input_shape = (self.settings.IMG_WIDTH, self.settings.IMG_WIDTH, 3)
        # base_model = EfficientNetClass(include_top=False, weights=None, input_shape=input_shape)
        input_tensor = Input(shape=(self.settings.IMG_WIDTH, self.settings.IMG_WIDTH, 3))
        base_model = EfficientNetB0(input_tensor=input_tensor, weights='imagenet', include_top=False)
        
        # add a global spatial average pooling layer
        x = base_model.output
        x = GlobalAveragePooling2D()(x)
        # let's add a fully-connected layer
        x = Dense(1024, activation='relu')(x)
        # and a logistic layer -- let's say we have 200 classes
        num_classes = self.settings.NUM_CLASSES
        predictions = Dense(num_classes, activation='softmax')(x)

        # this is the model we will train
        model = Model(inputs=base_model.input, outputs=predictions)

        model.load_weights(self.settings.MODEL_PATH)

        return model


    def imread(self, file_path) :
        stream = open( file_path.encode("utf-8") , "rb")
        bytes = bytearray(stream.read())
        np_array = np.asarray(bytes, dtype=np.uint8)
        return cv2.imdecode(np_array , cv2.IMREAD_UNCHANGED)

    # PIL Image(RGB) -> Numpy Array(BGR)
    def to_image_np(self, image):
        if type(image) == np.ndarray: return image

        return np.array(image)[...,::-1]

    def predict_files(self, file_path):

        image_np = self.imread(file_path)
        return self._predict(image_np)

    def predict_image(self, image):

        image_np = self.to_image_np(image)
        return self.predict(image_np)


    def predict(self, image_np):

        # print(self.model.input_shape)    # (None, 224, 224, 3)
        # print(type(image_np))
        # print(image_np.shape)            # (52, 22, 3)
        image_np = cv2.resize(image_np, dsize=(self.model.input_shape[2], self.model.input_shape[1]), interpolation=cv2.INTER_CUBIC)

        x = preprocess_input(image_np)
        x = np.expand_dims(x, axis=0)

        preds = self.model.predict(x)
        # print(preds)
        prob = np.max(preds)
        class_id = np.argmax(preds)
        # class_text = self.settings.CLASS_DICT[str(class_id)]

        # print(class_id, class_text, prob)
        return (class_id, prob)


    def predict_ranking(self, image_np, ranking_count = 3, thredhold = 0.1):

        # print(self.model.input_shape)    # (None, 224, 224, 3)
        # print(image_np.shape)            # (52, 22, 3)
        image_np = cv2.resize(image_np, dsize=(self.model.input_shape[2], self.model.input_shape[1]), interpolation=cv2.INTER_AREA)

        x = preprocess_input(image_np)
        x = np.expand_dims(x, axis=0)

        preds = self.model.predict(x)

        # print(preds)
        # ret = decode_predictions(y)
        # prob = np.sort(preds)
        ranking_index = np.argsort(preds)
        # print(ranking_index)
        ranking_list = []
        for i in range(1, ranking_count +1):
            class_id = ranking_index[0][-1 * i]
            # class_text = self.settings.CLASS_DICT[str(class_id)]
            prob = float(preds[0][class_id])
            if prob >= thredhold:
                ranking_list.append({
                    "class_id": class_id,
                    # "class_text": class_text,
                    "prob": prob,
                    })

        return ranking_list


    def predict_batch(self, images):
        pred_list=[]
        # for index, file in enumerate(image_list):
        #     print(index, file)
        #     image_path = os.path.join(FOLDER_PATH, file)

        #     img = image.load_img(image_path, target_size=(224, 224))
        #     img = image.img_to_array(img)
        #     img = preprocess_input(img)
        #     img = np.expand_dims(img, axis=0)
        #     # normalize the image
        #     # processed_image = np.array(img, dtype="float") / 255.0
        #     processed_image = np.array(img, dtype="float")
        #     images.append(processed_image)
            
        images = np.vstack(images)
        preds = self.model.predict(images)
        preds = np.array(preds)

        prob_list = np.max(preds, axis=1)
        class_id_list = np.argmax(preds, axis=1)
        pred_list.append(class_id_list)
        #return (class_id_list, prob_list)
        return pred_list




    def predict_batch_ranking(self, images, ranking_count = 3, thredhold = 0.1):

        # FOLDER_PATH = r"E:\DATA\@car\carphoto\car_object_crop\0"
        # images = []
        # image_list = os.listdir(FOLDER_PATH)[:10]

        # for index, file in enumerate(image_list):
        #     print(index, file)
        #     image_path = os.path.join(FOLDER_PATH, file)

        #     img = image.load_img(image_path, target_size=(224, 224))
        #     img = image.img_to_array(img)
        #     img = preprocess_input(img)
        #     img = np.expand_dims(img, axis=0)
        #     # normalize the image
        #     # processed_image = np.array(img, dtype="float") / 255.0
        #     processed_image = np.array(img, dtype="float")
        #     images.append(processed_image)
            
        images_np = np.vstack(images)
        preds = self.model.predict(images_np)
        preds = np.array(preds)
        # print(preds)
        # print(preds.shape)

        ranking_index = np.argsort(preds, axis=1)

        ranking_list = []
        for i in range(len(images)):

            ranking = []
            for j in range(1, ranking_count +1):
                class_id = ranking_index[i][-1 * j]
                # class_text = self.settings.CLASS_DICT[str(class_id)]
                prob = float(preds[i][class_id])
                # print(j, class_id, prob)
                if prob >= thredhold:
                    ranking.append({
                        "class_id": class_id,
                        # "class_text": front_class_dict[str(class_id)],
                        "prob": prob,
                        })
            # print(i, image_list[i], ranking)

            ranking_list.append(ranking)

        return ranking_list


if __name__ == '__main__':

    import time
    import drone_util.image_util as image_util

    from model_data.car_brand import settings_brand as settings 
    helper = EFNetHelper(settings.CAR_BRAND_FRONT)


    FOLDER_PATH = "./data/image/Crop"

    for file in os.listdir(FOLDER_PATH):
        image_path = os.path.join(FOLDER_PATH, file)
        
        start = time.time()
        image_np = image_util.imread(image_path)
        result_list = helper.predict_ranking(image_np)
        interval = time.time() - start
        print(interval, result_list)

    exit()

    start = time.time()
    class_id, prob = helper.predict_batch()
    interval = time.time() - start
    print(interval, class_id, prob)
    exit()
 
    # BASE_FOLDER_PATH = r"D:\DATA\@car\car_photo\carphoto_20190627"
    # BASE_FOLDER_PATH = r"D:\DATA\@car\car_brand_bobaedream\20190703\GRANDEUR"
    # BASE_FOLDER_PATH = r"D:\DATA\@car\car_brand_encar\5\현대_그랜저_더 뉴 그랜저 IG"
    BASE_FOLDER_PATH = r"E:\TEMP\carplate"
    # img_path = r"E:\DATA\@car\carphoto\carphoto_20190614\ARHF6029.jpg"

    files = os.listdir(BASE_FOLDER_PATH)
    for i, filename in enumerate(files):
        file_path = os.path.join(BASE_FOLDER_PATH, filename)
        start = time.time()
        ret = helper.predict(file_path)
        # print(ret)

        end = time.time()
        seconds = end - start

        print(
            filename,
            settings.CLASS_DICT[str(ret[0])],
            ret[1],
            seconds
        )


