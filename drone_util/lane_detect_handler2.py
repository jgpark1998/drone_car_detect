import cv2
import numpy as np
import matplotlib.pyplot as plt
import json
import time

from numpy.lib.function_base import copy

import drone_util.lane_utils as lane_utils
import drone_util.common_util as common_util
import drone_util.image_util as image_util
import os
from config import *

#initialize color map
cmap = plt.get_cmap('tab20b')
colors = [cmap(i)[:3] for i in np.linspace(0, 1, 20)]

class LaneDetectHandler:

    # 유효한 frame_index를 지정함 (return False 되는 frame_index는 처리안하고 SKIP함)
    @staticmethod
    def on_frame_valid(frame_index):
        if frame_index % FRAME_UNIT == 0:
            return True
        else:
            return False

    def __init__(self, model_path, sort_threshold, test_frame_num, clustering_threshold):
        self.start_flag = False
        self.result_index = 0
        self.land_dict = {} # ?
        self.moving_threshold = MOVING_THRESHOLD

        self.MODEL_PATH = model_path
        self.CLUSTERING_THRESHOLD = clustering_threshold
        self.SORT_THRESHOLD = sort_threshold
        self.TEST_FRAME_NUM = test_frame_num
        
        self.frame_width = None
        self.frame_height = None
        self.visualized_img_path = 'temp/linear_regression_result.jpg'
        self.visualized_on_frame_path = 'temp/linear_regression_result_added_on_frame.jpg'
        self.temp_forward_centers = []
        self.temp_backward_centers = []

    
    def start(self, force_retrain = False):
        self.land_dict = {}
        self.start_flag = True

        self.roi = None
        self.prev_result = {}
        self.accumulated_num = 0
        self.forward_info = [] # {'center' : , 'x_inter': }, {}, {} ... 
        self.backward_info = [] # {'center' : , 'x_inter': }, {}, {} ... 
        self.lane_line_result = {'forward':[], 'backward':[]} 
        self.flow_drawing = np.zeros((1080,1920,3), dtype=np.uint8)
        self.temp_forward_centers = []
        self.temp_backward_centers = []

        if force_retrain == False:
            if not os.path.exists(self.MODEL_PATH):  # model이 없으면 None리턴
                return None
            
            self.stop() # 학습 모델이 존재할 때는 start_flag = False로 학습 중지
            self.lane_line_result = common_util.load_json(self.MODEL_PATH)
            print('json result: ', self.lane_line_result)

        else:  # r키 눌렀을 경우 (json 파일이 있어도 새로 학습)
            print("Force Retrain")

    def stop(self):
        self.result_index = 0
        self.start_flag = False
    
        self.accumulated_num = 0
        self.forward_info = [] # {'center' : , 'x_inter': }, {}, {} ... 
        self.backward_info = [] # {'center' : , 'x_inter': }, {}, {} ... 
        self.flow_drawing = np.zeros((1080,1920,3), dtype=np.uint8)
        self.temp_forward_centers = []
        self.temp_backward_centers = []

    def is_start(self):
        return self.start_flag
    
    def set_roi(self, roi_rect):
        self.roi = roi_rect #[x1,y1,x2,y2]
    
    def is_on_first_lane(self, car_point, frame_shape):
        # calc distance (car center point & lane)
        frame_height, frame_width, _ = frame_shape
        lane_distances = []

        car_x, car_y = car_point[0], frame_height - car_point[1] # convert coord
        for lane in self.lane_line_result['forward']:
            distance = lane_utils.calc_dist_point_line((car_x, car_y), lane['bottom'], lane['top'])
            lane_distances.append(distance)

        first_line_distance = lane_distances[-1]

        for lane in self.lane_line_result['backward']:
            distance = lane_utils.calc_dist_point_line((car_x, car_y), lane['bottom'], lane['top'])
            lane_distances.append(distance)

        if lane_distances == []:
            print("No Lane Result")

        if first_line_distance == min(lane_distances):
            return True
        
        else:
            return False

    def map_prev_curr_cars(self, result_obj_list):
        mapped_result = []
        for current_item in result_obj_list:
            track_id = current_item["track_id"]
            # car_type = current_item["car_type"]
            # box = current_item["box"]

            for prev_item in self.prev_result:
                if prev_item["track_id"] == track_id:
                    mapped_result.append([prev_item, current_item])
                    break
        
        return mapped_result

    def save_optical_flow_element(self, mapped_result):
        # TODO: save temp centers
        for mapped_set in mapped_result:
            prev, curr = mapped_set
            track_id = prev["track_id"]
            prev_box = prev["box"]
            curr_box = curr["box"]
            prev_x, prev_y, prev_w, prev_h = prev_box["x"], prev_box["y"], prev_box["w"], prev_box["h"]
            prev_center = [prev_x + prev_w//2, prev_y + prev_h//2]
            curr_x, curr_y, curr_w, curr_h = curr_box["x"], curr_box["y"], curr_box["w"], curr_box["h"]
            curr_center = [curr_x + curr_w//2, curr_y + curr_h//2]

            vec = [curr_center[0]-prev_center[0], curr_center[1]-prev_center[1]]
            if vec != [0, 0]:
                angle = lane_utils.get_angle(vec)

                ''' forward '''
                if 0 < angle < 180:
                    if np.sqrt((prev_x-curr_x)**2 + (prev_y-curr_y)**2) > self.moving_threshold:
                        # to get x_interception
                        converted_coord_center1 = [prev_x, self.frame_height - prev_y]
                        converted_coord_center2 = [curr_x, self.frame_height - curr_y]
                        x_inter = lane_utils.get_x_intercept_in_roi(converted_coord_center1, converted_coord_center2, self.roi, self.frame_height)
                        if x_inter != None:
                            self.forward_info.append({'center':curr_center, 'x_inter':x_inter}) ## box_center & x_intercept
                            self.temp_forward_centers.append(curr_center)
                            # self.forward_info.append([curr_center, x_inter]) ## box_center & x_intercept

                ''' backward '''
                if 180 < angle < 360:
                    if np.sqrt((prev_x-curr_x)**2 + (prev_y -curr_y)**2) > self.moving_threshold:
                        # to get x_interception
                        converted_coord_center1 = [prev_x, self.frame_height - prev_y]
                        converted_coord_center2 = [curr_x, self.frame_height - curr_y]
                        x_inter = lane_utils.get_x_intercept_in_roi(converted_coord_center1, converted_coord_center2, self.roi, self.frame_height)
                        if x_inter != None:
                            self.backward_info.append({'center':curr_center, 'x_inter':x_inter})
                            self.temp_backward_centers.append(curr_center)
                            # self.backward_center_x_intercept.append([curr_center, x_inter]) ## box_center & x_intercept
    
    def group_by_forward_x(self):
        forward_center_groups = []
        max_x_forward = 0
        # if self.forward_center_x_intercept:
        result = []
        temp = []
        ## group by x_intercept
        self.forward_info = sorted(self.forward_info, key = lambda info:info['x_inter']) # x[1] : x intercept
        # print(self.forward_center_x_intercept)
        for info in self.forward_info:
            center, x_intercept = info['center'], info['x_inter']
            if x_intercept > max_x_forward:
                max_x_forward = x_intercept
            if temp:
                if abs(np.mean([t[1] for t in temp]) - x_intercept) < self.SORT_THRESHOLD:
                    temp.append([center, x_intercept])

                else: # 다른 그룹으로 분류
                    result.append(temp) # 이전까지의 결과 저장
                    temp = [[center, x_intercept]] # 초기화

            else: # 맨 처음 초기화
                temp.append([center, x_intercept])

        for res in result:
            forward_center_groups.append([r[0] for r in res])

        print("num of forward groups: ", len(forward_center_groups))

        return forward_center_groups, max_x_forward

    def linear_regression(self, centers):
            x = np.array([center[0] for center in centers])
            y = np.array([self.frame_height-center[1] for center in centers])
            A = np.vstack([x, np.ones(len(x))]).T

            m, c = np.linalg.lstsq(A, y, rcond=None)[0] # y = m*x+c 
            origin_point1 = (int(((self.frame_height-self.roi[3])+(-1)*c)//m), int(self.frame_height-self.roi[3]))
            origin_point2 = (0, int(c))
            # roi 내 직선
            # point_1 = (int(self.roi[0]), int(m*self.roi[0])+c)
            # point_2 = (int(self.roi[2]), int(m*self.roi[2])+c)
            # point_1 = (int(((self.frame_height-self.roi[3])+(-1)*c)//m), int(self.frame_height-self.roi[3]))
            # point_2 = (int(((self.frame_height-self.roi[1])+(-1)*c)//m), int(self.frame_height-self.roi[1]))

            return origin_point1, origin_point2

    # 매 프레임 호출됨 (인식 결과를 보내고 결과 화면을 리턴함)
    def on_frame_result(self, frame_index, frame_image, result_obj_list):
        self.result_index += 1
        self.frame_height, self.frame_width, self.frame_ch = frame_image.shape

        ''' [ 차량 detection 결과 시각화 ]'''
        for item in result_obj_list:
            track_id = item["track_id"]
            car_type = item["car_type"]
            box = item["box"]

            color = colors[track_id % len(colors)]
            color = [i * 255 for i in color]
            title = f"{track_id}|{car_type}"

            if lane_utils.is_box_in_roi(roi_box = self.roi, crop_box= box):
                cv2.rectangle(frame_image, (box["x"], box["y"]), (box["x"] + box["w"], box["y"] + box["h"]), color, 2)
            #     cv2.rectangle(frame_image, (box["x"], box["y"]-30), (box["x"]+(len(title)*17), box["y"]), color, -1)
            
            #     cv2.putText(frame_image, title + f"|{self.result_index}", (box["x"], box["y"]-10), 0, 0.75, (255,255,255), 2)
        ''' [ 차선 인식을 위한 data 저장 ] '''
        # 이전, 현재 결과를 매핑
        mapped_result = self.map_prev_curr_cars(result_obj_list)
        self.prev_result = result_obj_list

        # optical flow로 얻은 data 저장
        self.save_optical_flow_element(mapped_result)
        # -> self.forward_info, self.backward_info

        ''' [ 결과 영상에 feature 시각화 ] '''
        if TRACE_DRAWING:
            for c in self.temp_forward_centers:
                cv2.circle(frame_image, (int(c[0]), int(c[1])), 5, (255,127,0), -1)
            for c in self.temp_backward_centers:
                cv2.circle(frame_image, (int(c[0]), int(c[1])), 5, (0,255,255), -1)


        ''' [ TEST_FRAME_NUM이 되면 현재까지 data로 차선 인식 ]'''
        # 차선 인식이 완료되면 return True
        if self.result_index % self.TEST_FRAME_NUM == 0:
            self.accumulated_num += 1
            print("sort_threshold:", self.SORT_THRESHOLD)

            ''' < 1. Get Forward Line >'''
            ''' 1-1) Forward grouping''' # classify lanes
            # forward_center_groups, max_forward_x_inter = self.group_by_x_inter()

            # remove outliers
            forward_center_groups, max_forward_x_inter = [], 0
            if self.forward_info:
                print("before removing forward outlier:", len(self.forward_info))
                if len(self.forward_info) > self.CLUSTERING_THRESHOLD:
                    self.forward_info = lane_utils.remove_outliers(self.forward_info, key='x_inter')
                print("after removing forward outlier:", len(self.forward_info))
                forward_center_groups, max_forward_x_inter = self.group_by_forward_x()

            ''' 1-2) Forward linear regression '''
            prev_x_inter = None
            for center_group in forward_center_groups:
                if len(center_group) > self.CLUSTERING_THRESHOLD:
                    bottom, top = self.linear_regression(center_group)
                    
                    if prev_x_inter != None:
                        if abs(prev_x_inter - bottom[0]) > self.SORT_THRESHOLD:
                            self.lane_line_result['forward'].append({'bottom':bottom, 'top':top})
                            prev_x_inter = bottom[0]
                            # cv2.line(self.flow_drawing, (int(-1*c/m),0+self.frame_height), (0, self.frame_height-int(c)), (255, 127, 0), thickness=3)
                        # else:
                        #     # cv2.line(self.flow_drawing, (int(-1*c/m),0+self.frame_height), (0, self.frame_height-int(c)), (128,128,128), thickness=3)
                    else:
                        self.lane_line_result['forward'].append({'bottom':bottom, 'top':top})
                        # cv2.line(self.flow_drawing, (int(-1*c/m),0+self.frame_height), (0, self.frame_height-int(c)), (255, 127, 0), thickness=3)
                        prev_x_inter = bottom[0]

                else:
                    print("Not enough Forward cars")

            if self.lane_line_result['forward']:
                self.lane_line_result['forward'] = sorted(self.lane_line_result['forward'], key = lambda line: line['bottom'][0]) # roi bottom 라인에 닿는 x 좌표
                print("Forward sorted:", self.lane_line_result['forward'])

            else:
                print("No Forward Line Result")


            ''' < 2. Get Backward Line >''' # backward는 그냥 가장 왼쪽 차선만 검출
            ''' 2-1) Backward filtering'''
            self.backward_info = list(filter(lambda info:info['x_inter']>max_forward_x_inter, self.backward_info))
            if self.backward_info:
                # remove outliers
                print("before removing backward outlier:", len(self.backward_info))
                if len(self.backward_info) > self.CLUSTERING_THRESHOLD:
                    self.backward_info = lane_utils.remove_outliers(self.backward_info, key='x_inter')
                print("after removing backward outlier:", len(self.backward_info))


            ''' 2-2) Backward linear regression '''
            # sort
            backward_centers = []
            if self.backward_info:
                self.backward_info = sorted(self.backward_info, key = lambda info:info['x_inter']) # x[1] : x intercept
                for info in self.backward_info:
                    backward_centers.append(info['center'])

            # make backward line
            if len(backward_centers) > self.CLUSTERING_THRESHOLD:
                bottom, top = self.linear_regression(backward_centers)  # input : ot 'backward', backward_centers / return : x,y point
                self.lane_line_result['backward'].append({'bottom':bottom, 'top':top})
            
            else:
                print("Not enough backward cars")

            # center visualization
            lane_utils.draw_dots(self.flow_drawing, forward_center_groups, backward_centers)

            ''' < 3. Lane detection done > '''
            # (최소 앞차선 2개, 뒷차선 1개가 검출되었을 때 차선 인식 완료)
            if len(self.lane_line_result['forward'])>=2 and len(self.lane_line_result['backward'])>=1:
                # line visualization
                lane_utils.draw_lane_lines(self.flow_drawing, self.lane_line_result['forward'], self.lane_line_result['backward'], self.roi)
                cv2.imwrite(self.visualized_img_path, self.flow_drawing)
                cv2.imwrite(self.visualized_on_frame_path, cv2.add(frame_image, self.flow_drawing))
                # save lane result to json
                common_util.save_json(self.MODEL_PATH, self.lane_line_result)

                print("----------------------------------------------")
                print(f"Total Accumulated : {self.accumulated_num} ********************************************")
                self.stop()

                self.land_dict["result_index"] = self.result_index
                return self.land_dict

            else:
                cv2.imwrite(self.visualized_img_path, self.flow_drawing)
                cv2.imwrite(self.visualized_on_frame_path, cv2.add(frame_image, self.flow_drawing))
                self.lane_line_result = {'forward':[], 'backward':[]}
                self.flow_drawing = np.zeros((1080,1920,3), dtype=np.uint8)
                print(f"Accumulating ... {self.accumulated_num} ********************************************")

        return None


    def predict(self, frame_image, result_obj_list):
        line_image = frame_image.copy()
        frame_height, frame_width, _ = line_image.shape

        for item in result_obj_list:
            box = item["box"]
            x, y, w, h = box['x'], box['y'], box['w'], box['h']
            center = [x + w//2, y + h//2]

            # 1차선 위의 차량 구분
            item['is_on_first_lane'] = self.is_on_first_lane(center, line_image.shape)

        # 1차선 정보 return
        first_lane = self.lane_line_result['forward'][-1]
        roi_x1, roi_y1, roi_x2, roi_y2 = self.roi
        rect = (roi_x1, roi_y1, roi_x2-roi_x1, roi_y2-roi_y1) # to clip line
        print("first_line:", first_lane)
        
        point1 = (first_lane['bottom'][0], frame_height-first_lane['bottom'][1])
        point2 = (first_lane['top'][0], frame_height-first_lane['top'][1])
        ret, p1, p2 = cv2.clipLine(rect, point1, point2)
        line_point1 = (int(p1[0]), int(p1[1]))
        line_point2 = (int(p2[0]), int(p2[1]))
        
        return [line_point1, line_point2]
