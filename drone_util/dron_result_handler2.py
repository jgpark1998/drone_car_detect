import cv2
import numpy as np
import matplotlib.pyplot as plt
import drone_util.image_util as image_util
import random

# deep sort imports
from deep_sort import preprocessing, nn_matching
from deep_sort.detection import Detection
from deep_sort.tracker import Tracker
from tools import generate_detections as gdet

from model_data.car_brand import settings_brand as settings 
import drone_util.common_util as common_util

#initialize color map
cmap = plt.get_cmap('tab20b')
colors = [cmap(i)[:3] for i in np.linspace(0, 1, 20)]

def image_process(crop_np):
    img = cv2.resize(crop_np, 
                    dsize=(settings.CAR_BRAND_FRONT.IMG_WIDTH, settings.CAR_BRAND_FRONT.IMG_HEIGHT),
                    interpolation=cv2.INTER_CUBIC)

    img = np.expand_dims(img, axis=0)
    processed_image = np.array(img, dtype="float")
    
    return processed_image

def check_negative_bbox(crop_box):
    if crop_box["w"] <= 0 or crop_box["h"] <= 0:
        return True
    else:
        return False

def check_valid_roi(roi_box,crop_box):
    if crop_box["x"] < roi_box["x"] or \
    crop_box["y"] < roi_box["y"] or \
    (crop_box["w"] + crop_box["x"]) > (roi_box["w"] + roi_box["x"]) or \
    (crop_box["h"] + crop_box["y"]) > (roi_box["h"] + roi_box["y"]):
        return False
    else:
        return True


def convert_data_to_array_and_slice(valid_detections,boxes,scores,classes):
    num_objects = valid_detections.numpy()[0]
    bboxes = boxes.numpy()[0]
    bboxes = bboxes[0:int(num_objects)]
    scores = scores.numpy()[0]
    scores = scores[0:int(num_objects)]
    classes = classes.numpy()[0]
    classes = classes[0:int(num_objects)]

    return num_objects,bboxes,scores,classes

def check_allow_class(allowed_classes,class_names,classes,names,deleted_indx,num_objects,bboxes):
    for i in range(num_objects):
        class_indx = int(classes[i])
        class_name = class_names[class_indx]
        
        bbox = bboxes[i]

        if class_name not in allowed_classes:
            deleted_indx.append(i)
        elif bbox[0] < 1 or bbox[1] < 1:
            # bbox의 x, y 좌표가 양쪽 끝에 걸린 경우 제외 (화면안으로 진입중인 차량 제외)
            deleted_indx.append(i)
        else:
            names.append(class_name)
    names = np.array(names)
    count = len(names)

    return names, deleted_indx, count

def draw_crop_box_title(result_obj_list,frame_image):# TODO
    for item in result_obj_list:
        track_id = item["track_id"]
        car_type = item["car_type"]
        box = item["box"]

        color = colors[random.randint(1,100) % len(colors)]
        color = [i * 255 for i in color]

        title = f"{car_type}"
        
        # cv2.circle(frame_image, (int(self.max_x_inter), int(bottom)), 10, (255,0,0), -1)  # 프레임 상에 존재 하지 않아서 안 찍힘
        cv2.rectangle(frame_image, (box["x"], box["y"]), (box["x"] + box["w"], box["y"] + box["h"]), color, 2)
        cv2.rectangle(frame_image, (box["x"], box["y"]-30), (box["x"]+(len(title)*17), box["y"]), color, -1)
        
        cv2.putText(frame_image, title, (box["x"], box["y"]-10), 0, 0.75, (255,255,255), 2)
        # if track_id in self.cars_in_first_line:
        #     cv2.putText(frame_image, "is on the first line", (box["x"], box["y"]-20), 0, 0.75, (255,255,255), 2)

        # if track_id in self.cars_in_backward_first_line:
        #     cv2.putText(frame_image, "is on the backward line", (box["x"], box["y"]-20), 0, 0.75, (255,255,255), 2)

    return frame_image

def get_Deep_SORT_result(features,roi_box,frame,real_frame_size,bboxes,scores, names,tracker):
    
    # Deep_SORT parameter
    nms_max_overlap = 1.0

    # batch로 inference하기 위한 초기화
    result_obj_list = []
    images = []
    crop_boxes = []
    track_ids = []
    car_types = []
    detections = [Detection(bbox, score, class_name, feature) for bbox, score, class_name, feature in zip(bboxes, scores, names, features)]
    
    # 2.4 Detections initialize
    boxs = np.array([d.tlwh for d in detections])
    scores = np.array([d.confidence for d in detections])
    classes = np.array([d.class_name for d in detections])
    indices = preprocessing.non_max_suppression(boxs, classes, nms_max_overlap, scores)
    detections = [detections[i] for i in indices]   

    # Call the tracker
    tracker.predict()
    tracker.update(detections)  

    # update tracks
    for track in tracker.tracks:
        if not track.is_confirmed() or track.time_since_update > 1:
            continue 
        bbox = track.to_tlbr()
        class_name = track.get_class()
        # action class 체크
        track_id = int(track.track_id)
        obj_id = class_name + "-" + str(track.track_id)
        obj_index = track.track_id           

        sort_box = (int(bbox[0]), int(bbox[1]), int(bbox[2]), int(bbox[3]))


        crop_box = {
            "x": sort_box[0],
            "y": sort_box[1],
            "w": sort_box[2] - sort_box[0],
            "h": sort_box[3] - sort_box[1],
        }

        # 아래와 같은 경우를 대비
        # {'x': 1120, 'y': 308, 'w': -20, 'h': 24}
        if check_negative_bbox(crop_box):
            continue
        
        #  유효한 BBOX만 차선 검출에 넘김
        if check_valid_roi(roi_box, crop_box) == False:
            continue

        crop_np = image_util.crop_image(frame, crop_box)
        processed_image = image_process(crop_np)


        # 출력할 것
        images.append(processed_image)
        crop_boxes.append(crop_box)
        track_ids.append(obj_index)
        car_types.append(class_name)


        result_obj = {
            "track_id": track_id,
            "car_type": class_name,
            "box": crop_box,
            'image':processed_image
        }

        result_obj_list.append(result_obj)

    return result_obj_list


def get_only_YOLO_result(roi_box,frame,real_frame_size,bboxes,names):
    images = []
    crop_boxes = []
    track_ids = []
    car_types = []
    result_obj_list = []    
    for bbox, class_name in zip(bboxes,names):
        
        yolo_box = (int(bbox[0]), int(bbox[1]), int(bbox[2]), int(bbox[3]))
        obj_index = None
        crop_box = {
        "x": yolo_box[0],
        "y": yolo_box[1],
        "w": yolo_box[2], 
        "h": yolo_box[3] 
    }
        #crop_box = image_util.expand_box(crop_box, 5, real_frame_size[1], real_frame_size[0])
        # 아래와 같은 경우를 대비
        # {'x': 1120, 'y': 308, 'w': -20, 'h': 24}
        if check_negative_bbox(crop_box):
            continue

        crop_np = image_util.crop_image(frame, crop_box)

        # crop된 차 이미지의 모든 좌표가 ROI 안에 존재 하지 않을 경우에 inference 생략
        if check_valid_roi(roi_box, crop_box) == False:
            continue

        processed_image = image_process(crop_np)

        # 출력할 것
        images.append(processed_image)
        crop_boxes.append(crop_box)
        track_ids.append(obj_index)
        car_types.append(class_name)

        result_obj = {
        "track_id": None,
        "car_type": class_name,
        "box": crop_box,
        'image':processed_image
        }

        result_obj_list.append(result_obj)

    return result_obj_list