import drone_util.common_util as common_util

# 2차선, 1차선, 반대차선 순으로 넣어야 합니다
# bottom top 순으로 넣어야 합니다 (x축이 큰 순서대로)

############ 그림판 좌표 ##################

# 1번 영상
# second_lane = [[1220, 800], [500, 500]]

# first_lane = [[1300, 700], [500,450]]

# back_lane = [[1300, 590], [500,400]]

# 2번 영상
second_lane = [[1100, 800], [635, 250]]

first_lane = [[1300, 680], [715, 212]]

back_lane = [[1300, 500], [880,250]]

################## cv2 계산된 변환 좌표 ##################
HEIGHT = 1080

second_lane[0][1] , second_lane[1][1] = HEIGHT - second_lane[0][1], HEIGHT - second_lane[1][1]

first_lane[0][1] , first_lane[1][1] = HEIGHT - first_lane[0][1], HEIGHT - first_lane[1][1]

back_lane[0][1] , back_lane[1][1] = HEIGHT - back_lane[0][1], HEIGHT - back_lane[1][1]

################## make lane dict #############################

second_lane_dict = {'bottom' : second_lane[0] , 'top': second_lane[1]}

first_lane_dict = {'bottom' : first_lane[0] , 'top': first_lane[1]}

back_lane_dict = {'bottom' : back_lane[0] , 'top': back_lane[1]}



################### make dict to list ###########################

forward_list = [second_lane_dict , first_lane_dict]
backward_list = [back_lane_dict]


####################### lane_line_result ########################

lane_line_result = {'forward': forward_list , 'backward': backward_list}


print(lane_line_result)
common_util.save_json('lane_detect.json', lane_line_result)

