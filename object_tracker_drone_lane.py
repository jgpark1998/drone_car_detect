import os
# comment out below line to enable tensorflow logging outputs
os.environ["CUDA_VISIBLE_DEVICES"] = "1"
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import time
import tensorflow as tf
physical_devices = tf.config.experimental.list_physical_devices('GPU')
if len(physical_devices) > 0:
    tf.config.experimental.set_memory_growth(physical_devices[0], True)
    tf.config.experimental.set_visible_devices(physical_devices[0], 'GPU')
from absl import app, flags, logging
from absl.flags import FLAGS
import core.utils as utils
from core.yolov4 import filter_boxes
from tensorflow.python.saved_model import tag_constants
from core.config import cfg
from PIL import Image
import cv2
import numpy as np
import matplotlib.pyplot as plt
from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession
# deep sort imports
from deep_sort import preprocessing, nn_matching
from deep_sort.detection import Detection
from deep_sort.tracker import Tracker
from tools import generate_detections as gdet

# from action_class_helper import ActionClassHelper
# from car_type_helper import CarTypeHelper
from not_use.opencv_ffmpeg import FFmpegVideoCapture

# import result_handler_drone_lane as result_handler

from drone_util.lane_detect_handler2 import LaneDetectHandler ###

import drone_util.common_util as common_util
import drone_util.image_util as image_util

flags.DEFINE_string('framework', 'tf', '(tf, tflite, trt')
flags.DEFINE_string('weights', './checkpoints/yolov4-608',
                    'path to weights file')
flags.DEFINE_integer('size', 608, 'resize images to')
flags.DEFINE_boolean('tiny', False, 'yolo or yolo-tiny')
flags.DEFINE_string('model', 'yolov4', 'yolov3 or yolov4')
flags.DEFINE_string('video', './data/video/test.mp4', 'path to input video or set to 0 for webcam')
flags.DEFINE_string('output', None, 'path to output video')
flags.DEFINE_string('output_format', 'MP4V', 'codec used in VideoWriter when saving video to file')
flags.DEFINE_float('iou', 0.45, 'iou threshold')
flags.DEFINE_float('score', 0.50, 'score threshold')
flags.DEFINE_boolean('dont_show', False, 'dont show video output')
flags.DEFINE_boolean('info', False, 'show detailed info of tracked objects')
flags.DEFINE_boolean('count', False, 'count objects being tracked on screen')



ROI_DICT = {
    "DJI_0004.mp4": (500, 400, 1500, 900),
    "20210209_050631.MP4" : (500, 400, 1500, 900),
    "20210209_051312.MP4" : (500, 400, 1500, 900),
    "DRON_VIDEO_4.mp4" : (500, 400, 1500, 900)
}



def get_masked_image(image_np):
    ROI_LINE = (290, 740)
    y1, y2 = ROI_LINE

    height, width, _ = image_np.shape
    mask = np.zeros(image_np.shape, np.uint8)

    points = [(0, y1), (width, y2), (width, height), (0, width)]
    points = np.array(points, np.int32)
    points = points.reshape((-1, 1, 2))

    mask = cv2.polylines(mask, [points], True, (255, 255, 255), 2)
    mask2 = cv2.fillPoly(mask.copy(), [points], (255, 255, 255))
    mask3 = cv2.fillPoly(mask.copy(), [points], (0, 255, 0))

    show_image = cv2.addWeighted(src1=image_np, alpha=0.8, src2=mask3, beta=0.2, gamma=0)

    roi_image = cv2.bitwise_and(mask2, image_np)
    return roi_image


# obj_id의 index를 저장 (index는 현재 obj_id_dict의 len 값으로 결정)
obj_id_dict = {}

def get_obj_index(obj_id):
    global obj_id_dict

    if obj_id in obj_id_dict:
        obj_index = obj_id_dict[obj_id]
    else:
        obj_index = len(obj_id_dict)
        obj_id_dict[obj_id] = obj_index

    print(obj_id_dict)
    return f"{obj_index}_{obj_id}"


# ROI 내부 좌표를 전체 이미지 영역 좌표로 변환
def trans_roi_rect(roi_rect, bbox):
    new_bbox = []
    new_bbox.append(bbox[0] + roi_rect[0])
    new_bbox.append(bbox[1] + roi_rect[1])
    new_bbox.append(bbox[2] + roi_rect[0])
    new_bbox.append(bbox[3] + roi_rect[1])

    return new_bbox



def log_image(frame_index, image_np):
    log_file = f"{frame_index}.jpg"
    log_path = os.path.join("temp", "log_image", log_file)
    cv2.imwrite(log_path, image_np)

laneDetectHandler = LaneDetectHandler()
laneDetectHandler.start()

# carTypeHelper = CarTypeHelper()

# from efnet_client import EFNetClient
# efnetClient = EFNetClient("cartype")

# from carplate_client import CarplateClient
# carplateClient = CarplateClient()

def main(_argv):
    # Definition of the parameters
    max_cosine_distance = 0.4
    nn_budget = None
    nms_max_overlap = 1.0
    
    # initialize deep sort
    model_filename = 'model_data/mars-small128.pb'
    encoder = gdet.create_box_encoder(model_filename, batch_size=1)
    # calculate cosine distance metric
    metric = nn_matching.NearestNeighborDistanceMetric("cosine", max_cosine_distance, nn_budget)
    # initialize tracker
    tracker = Tracker(metric)

    # load configuration for object detector
    config = ConfigProto()
    config.gpu_options.allow_growth = True
    session = InteractiveSession(config=config)
    STRIDES, ANCHORS, NUM_CLASS, XYSCALE = utils.load_config(FLAGS)
    input_size = FLAGS.size
    video_path = FLAGS.video

    # load tflite model if flag is set
    if FLAGS.framework == 'tflite':
        interpreter = tf.lite.Interpreter(model_path=FLAGS.weights)
        interpreter.allocate_tensors()
        input_details = interpreter.get_input_details()
        output_details = interpreter.get_output_details()
        print(input_details)
        print(output_details)
    # otherwise load standard tensorflow saved model
    else:
        saved_model_loaded = tf.saved_model.load(FLAGS.weights, tags=[tag_constants.SERVING])
        infer = saved_model_loaded.signatures['serving_default']

    # begin video capture
    # try:
    #     vid = cv2.VideoCapture(int(video_path))
    # except:
    #     vid = cv2.VideoCapture(video_path)

    vid = cv2.VideoCapture(video_path)
    width = int(vid.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT))

    vid_ffmpeg = FFmpegVideoCapture(video_path, width, height, "bgr24")
    
    out = None

    # get video ready to save locally if flag is set
    if FLAGS.output:
        # by default VideoCapture returns float instead of int
        width = int(vid.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT))
        # fps = int(vid.get(cv2.CAP_PROP_FPS))
        fps = 10
        codec = cv2.VideoWriter_fourcc(*FLAGS.output_format)
        out = cv2.VideoWriter(FLAGS.output, codec, fps, (width, height))

    # print("CAP_PROP_ORIENTATION_META:", vid.get(cv2.CAP_PROP_ORIENTATION_META))
    # print("CAP_PROP_ORIENTATION_AUTO:", vid.get(cv2.CAP_PROP_ORIENTATION_AUTO))

    # return

    # actionClassHelper = ActionClassHelper(fps=30)

    # 결과 dict (key: obj_id)
    report_dict = {} 

    frame_num = -1
    # while video is running


    while True:
        frame_num +=1
        
        # return_value, frame = vid.read()
        return_value, frame = vid_ffmpeg.read()
        frame_height, frame_width, _ = frame.shape


        if return_value:
            # masked_frame = get_masked_image(frame)
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            # frame=cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE)
            # image = Image.fromarray(frame)

            # ROI 적용
            video_file = os.path.basename(video_path)
            if video_file in ROI_DICT:
                roi_rect = ROI_DICT[video_file]
                laneDetectHandler.set_roi(roi_rect) ##
                roi_box = {
                    "x": roi_rect[0],
                    "y": roi_rect[1],
                    "w": roi_rect[2] - roi_rect[0],
                    "h": roi_rect[3] - roi_rect[1],
                }
                frame_roi = image_util.crop_image(frame, roi_box)
            else:
                frame_roi = frame 

        else:
            print('Video has ended or failed, try a different video format!')
            break
        print('Frame #: ', frame_num)

        if LaneDetectHandler.on_frame_valid(frame_num) == False:
            frame_num += 1
            continue

        # if frame_num % 3 != 0:
        #     frame_num += 1
        #     continue

        if frame_num < 300:
            frame_num += 1
            continue

        # if frame_num >= 30 * 60:   # 1분까지만 유효
        #     break


        frame_size = frame_roi.shape[:2]
        image_data = cv2.resize(frame_roi, (input_size, input_size))
        # cv2.imshow("image_data", image_data)

        image_data = image_data / 255.
        image_data = image_data[np.newaxis, ...].astype(np.float32)
        start_time = time.time()

        # run detections on tflite if flag is set
        if FLAGS.framework == 'tflite':
            interpreter.set_tensor(input_details[0]['index'], image_data)
            interpreter.invoke()
            pred = [interpreter.get_tensor(output_details[i]['index']) for i in range(len(output_details))]
            # run detections using yolov3 if flag is set
            if FLAGS.model == 'yolov3' and FLAGS.tiny == True:
                boxes, pred_conf = filter_boxes(pred[1], pred[0], score_threshold=0.25,
                                                input_shape=tf.constant([input_size, input_size]))
            else:
                boxes, pred_conf = filter_boxes(pred[0], pred[1], score_threshold=0.25,
                                                input_shape=tf.constant([input_size, input_size]))
        else:
            batch_data = tf.constant(image_data)
            pred_bbox = infer(batch_data)
            for key, value in pred_bbox.items():
                boxes = value[:, :, 0:4]
                pred_conf = value[:, :, 4:]

        boxes, scores, classes, valid_detections = tf.image.combined_non_max_suppression(
            boxes=tf.reshape(boxes, (tf.shape(boxes)[0], -1, 1, 4)),
            scores=tf.reshape(
                pred_conf, (tf.shape(pred_conf)[0], -1, tf.shape(pred_conf)[-1])),
            max_output_size_per_class=50,
            max_total_size=50,
            iou_threshold=FLAGS.iou,
            score_threshold=FLAGS.score
        )

        # print(classes)

        # convert data to numpy arrays and slice out unused elements
        num_objects = valid_detections.numpy()[0]
        bboxes = boxes.numpy()[0]
        bboxes = bboxes[0:int(num_objects)]
        scores = scores.numpy()[0]
        scores = scores[0:int(num_objects)]
        classes = classes.numpy()[0]
        classes = classes[0:int(num_objects)]

        # format bounding boxes from normalized ymin, xmin, ymax, xmax ---> xmin, ymin, width, height
        original_h, original_w, _ = frame_roi.shape
        bboxes = utils.format_boxes(bboxes, original_h, original_w)


        # store all predictions in one parameter for simplicity when calling functions
        pred_bbox = [bboxes, scores, classes, num_objects]

        # read in all class names from config
        class_names = utils.read_class_names(cfg.YOLO.CLASSES)

        # by default allow all classes in .names file
        # allowed_classes = list(class_names.values())
        
        # custom allowed classes (uncomment line below to customize tracker for only people)
        allowed_classes = ['car', 'bus', 'truck']

        # loop through objects and use class index to get class name, allow only classes in allowed_classes list
        names = []
        deleted_indx = []
        for i in range(num_objects):
            class_indx = int(classes[i])
            class_name = class_names[class_indx]

            bbox = bboxes[i]
            
            if class_name not in allowed_classes:
                deleted_indx.append(i)
            elif bbox[0] < 3:
                # bbox가 양쪽 끝에 걸린 경우 제외
                deleted_indx.append(i)
            else:
                names.append(class_name)
        names = np.array(names)
        count = len(names)
        if FLAGS.count:
            cv2.putText(frame, "Objects being tracked: {}".format(count), (5, 35), cv2.FONT_HERSHEY_COMPLEX_SMALL, 2, (0, 255, 0), 2)
            print("Objects being tracked: {}".format(count))
        # delete detections that are not in allowed_classes
        bboxes = np.delete(bboxes, deleted_indx, axis=0)
        scores = np.delete(scores, deleted_indx, axis=0)

        # encode yolo detections and feed to tracker
        features = encoder(frame, bboxes)
        detections = [Detection(bbox, score, class_name, feature) for bbox, score, class_name, feature in zip(bboxes, scores, names, features)]

        #initialize color map
        cmap = plt.get_cmap('tab20b')
        colors = [cmap(i)[:3] for i in np.linspace(0, 1, 20)]

        # run non-maxima supression
        boxs = np.array([d.tlwh for d in detections])
        scores = np.array([d.confidence for d in detections])
        classes = np.array([d.class_name for d in detections])
        indices = preprocessing.non_max_suppression(boxs, classes, nms_max_overlap, scores)
        detections = [detections[i] for i in indices]       

        # Call the tracker
        tracker.predict()
        tracker.update(detections)

        result_obj_list = []

        # update tracks
        for track in tracker.tracks:
            if not track.is_confirmed() or track.time_since_update > 1:
                continue 
            bbox = track.to_tlbr()
            class_name = track.get_class()

        # car type 체크
            track_id = int(track.track_id)

            obj_id = class_name + "-" + str(track.track_id)
            # obj_index = get_obj_index(obj_id)
            obj_index = track.track_id

            # ROI 좌표를 전체 좌표에 맞춤
            bbox = trans_roi_rect(roi_rect, bbox)


            action_box = (int(bbox[0]), int(bbox[1]), int(bbox[2]), int(bbox[3]))
            # ret = actionClassHelper.put(track_id, action_box, frame) 
            # print(track_id, action_box, "==>", ret)

            crop_box = {
                "x": action_box[0],
                "y": action_box[1],
                "w": action_box[2] - action_box[0],
                "h": action_box[3] - action_box[1],
            }
            
            # crop_box = image_util.expand_box(crop_box, 5, frame_size[1], frame_size[0])
            # print(crop_box)

            # 아래와 같은 경우를 대비
            # {'x': 1120, 'y': 308, 'w': -20, 'h': 24}
            if crop_box["w"] <= 0 or crop_box["h"] <= 0:
                continue
            
            
            result_obj = {
                "track_id": track_id,
                "car_type": class_name,
                "box": crop_box
            }

            #cv2.rectangle(frame, (int(crop_box['x']), int(crop_box['y'])), (int(crop_box['w'])+int(crop_box['x']), int(crop_box['h'])+int(crop_box['y'])), (0,255,0), 2)
            cv2.rectangle(frame, (action_box[0], action_box[1]), (action_box[2], action_box[3]), (0,255,0), 2)
            
            result_obj_list.append(result_obj)
            continue
            
            # crop_np = image_util.crop_image(frame_roi, crop_box)

            # 외부 API 이용시
            # b64_image = image_util.encode_base64_np(crop_np)
            # result = efnetClient.predict_base64(b64_image)
            # print(result)
            # class_text = result.get("class_text")
            # cartype_prob = result.get("prob")

            # 1. 차종 인식
            # class_id, class_text, cartype_prob = carTypeHelper.predict(crop_np)

            # 한글 -> 영문
            # class_text = CLASS_CONV_DICT.get(class_text, class_text)

            # obj_dict = report_dict.get(obj_index, {})
            # if len(obj_dict) == 0:
            #     obj_dict["frame_num"] = frame_num

            # obj_dict["box"] = crop_box

            # if cartype_prob > obj_dict.get("cartype_prob", 0):
            #     obj_dict["cartype_prob"] = cartype_prob
            #     obj_dict["cartype"] = class_text

            # 2. 번호판 인식
            carplate_text = None
            # carplate_result = []
            # # carplate_result = carplateClient.predict_base64(b64_image)
            # # print(carplate_result)
            # if len(carplate_result) > 0:
            #     carplate_dict = carplate_result[0]
            #     carplate_text = carplate_dict.get("text", "")
            #     carplate_prob = carplate_dict.get("prob", 0)
            #     if carplate_text:
            #         carplate_text = carplate_text[-4:]

            #         if carplate_prob > obj_dict.get("carplate_prob", 0):
            #             obj_dict["carplate_prob"] = carplate_prob
            #             obj_dict["carplate_text"] = carplate_text

            # if obj_dict:
            #     report_dict[obj_index] = obj_dict

            

        # draw bbox on screen
            color = colors[int(track.track_id) % len(colors)]
            color = [i * 255 for i in color]

            bbox = trans_roi_rect(roi_rect, bbox)

            # 2. 차선 인식
            lane = get_lane(track_id, int(bbox[0]), int(bbox[1]))

            cv2.rectangle(frame, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])), color, 2)
            cv2.rectangle(frame, (int(bbox[0]), int(bbox[1]-30)), (int(bbox[0])+(len(f"Lane{lane}") + len(class_name)+len(str(track.track_id)))*17, int(bbox[1])), color, -1)

            # if carplate_text:
            #     title = f"[Lane{lane}]{obj_index}-{class_text}({carplate_text})"
            # else:
            title = f"[Lane{lane}] {class_text}-{obj_index}"

            cv2.putText(frame, title, (int(bbox[0]), int(bbox[1]-10)),0, 0.75, (255,255,255),2)


        # if enable info flag then print details about each track
            if FLAGS.info:
                print("Tracker ID: {}, Class: {},  BBox Coords (xmin, ymin, xmax, ymax): {}".format(str(track.track_id), class_name, (int(bbox[0]), int(bbox[1]), int(bbox[2]), int(bbox[3]))))

        if laneDetectHandler.is_start():
            result = laneDetectHandler.on_frame_result(frame_num, frame, result_obj_list)

            if result:
                # 차선 인식 완료
                print("차선 인식 완료")
                # if laneDetectHandler.lane_line_result != None:
                #     line_point1 = (int(-1*laneDetectHandler.first_lane['c']//laneDetectHandler.first_lane['m']),0+frame_height)
                #     line_point2 = (0, int(frame_height-laneDetectHandler.first_lane['c']))
                #     cv2.line(frame, line_point1, line_point2, (0,0,255), thickness=5)
                

        else:
            # # 1차선을 계속 그려야함
            # line_points = laneDetectHandler.predict(frame, result_obj_list)
            line_points = laneDetectHandler.predict(frame, result_obj_list)
            print(line_points)
            line_point1, line_point2 = line_points
            cv2.line(frame, line_point1, line_point2, (0,0,255), thickness=5)
            # laneDetectHandler.start() ## 차선 업데이트 test
            # -> 위 함수 호출로 기존 car detection 결과에 is_on_first_lane 이라는 key 생성되었을 것
            print(result_obj_list)
            # TODO: efnet으로 Predict 인식된 차선으로 1차선 위의 차량 판단 나머지 흰색, 1차선 파란색 위반 빨간색

        # draw ROI
        if roi_rect:
            cv2.rectangle(frame, (roi_rect[0], roi_rect[1]), (roi_rect[2], roi_rect[3]), (255, 255, 255), 2)

        # log_image(frame_num, frame)
            

        # calculate frames per second of running detections
        fps = 1.0 / (time.time() - start_time)
        print("FPS: %.2f" % fps)
        result = np.asarray(frame)
        result = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
        
        if not FLAGS.dont_show:
            cv2.imshow("Output Video", result)
        
        # if output flag is set, save video file
        if FLAGS.output:
            # out.write(result)
            cv2.imwrite("temp/making.jpg", result)

        if cv2.waitKey(1) & 0xFF == ord('q'): break
        if cv2.waitKey(1) & 0xFF == ord('r'):
            if laneDetectHandler.is_start() == False:
                laneDetectHandler.start(True)
    cv2.destroyAllWindows()

    # report_file = "report_" + os.path.splitext(os.path.basename(video_path))[0] + ".json"
    # common_util.save_json(report_file, report_dict)
    
if __name__ == '__main__':
    try:
        app.run(main)
    except SystemExit:
        pass
