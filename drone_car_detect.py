import os
# comment out below line to enable tensorflow logging outputs
# os.environ["CUDA_VISIBLE_DEVICES"] = "0"
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import time
import tensorflow as tf
physical_devices = tf.config.experimental.list_physical_devices('GPU')
if len(physical_devices) > 0:
    tf.config.experimental.set_memory_growth(physical_devices[0], True)
    tf.config.experimental.set_visible_devices(physical_devices[0], 'GPU')
from absl import app, flags
from absl.flags import FLAGS
import core.utils as utils
from core.yolov4 import filter_boxes
from tensorflow.python.saved_model import tag_constants
from core.config import cfg
from PIL import Image, ImageFont, ImageDraw
import cv2
import numpy as np
import matplotlib.pyplot as plt
from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

# deep sort imports
from deep_sort import preprocessing, nn_matching
from deep_sort.tracker import Tracker
from tools import generate_detections as gdet

# from action_class_helper import ActionClassHelper

import drone_util.common_util as common_util
import drone_util.image_util as image_util
import drone_util.dron_result_handler2 as drh
from model_data.car_type import settings_car_type as settings 
from drone_util.efnet_helper_ver2 import EFNetHelper

# PLAN A
from drone_util.lane_detect_handler2 import LaneDetectHandler

# PLAN B
#from drone_util.lane_detect_handler_optical import LaneDetectHandler

# sideview class
from drone_util.sideview import SideViewHandler

# config로 FLAG 대체
from config import *

# 차선 학습 객체 생성
laneDetectHandler = LaneDetectHandler(model_path="lane_detect.json", sort_threshold= SORT_THRESHOLD, test_frame_num= TEST_FRAME_NUM, clustering_threshold= CLUSTERING_THRESHOLD)
laneDetectHandler.start()

# Side_view 객체 생성
side_view_handler = SideViewHandler()

# EFNet Helper 객체 선언
helper = EFNetHelper(settings)

def main(_argv):

    # 1. SORT 객체 생성, YOLO 설정, Video Capture 설정
    class_dict = common_util.load_json(settings.CLASS_TEXT_PATH)

    # 1-1. create instance of Deep SORT
    # Definition of the parameters
    max_cosine_distance = 0.8
    nn_budget = None
    nms_max_overlap = 1.0
    
    # initialize deep sort
    model_filename = 'model_data/mars-small128.pb'
    encoder = gdet.create_box_encoder(model_filename, batch_size=1)
    # calculate cosine distance metric
    metric = nn_matching.NearestNeighborDistanceMetric("cosine", max_cosine_distance, nn_budget)
    # initialize tracker
    tracker = Tracker(metric)


    # load configuration for object detector
    config = ConfigProto()
    config.gpu_options.allow_growth = True
    # session = InteractiveSession(config=config)
    input_size = SIZE


    # 1-2. load tflite model if flag is set

    saved_model_loaded = tf.saved_model.load(WEIGHTS, tags=[tag_constants.SERVING])
    infer = saved_model_loaded.signatures['serving_default']


    # 1-3. setting VideoCapture
    # vid = cv2.VideoCapture(video_path)
    # width = int(vid.get(cv2.CAP_PROP_FRAME_WIDTH))
    # height = int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT))


    # # 캡처카드 사용시
    vid = cv2.VideoCapture(0)
    width = 1920
    height = 1080


    frame_num = -1
    
    # 2. 매 frame 마다 YOLO, SORT, 실행 후 시각화 
    # while video is running
    while True:
        # 2.1 read video, apply masking, apply ROI
        frame_num +=1
        return_value, frame = vid.read()
        if return_value:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            frame_to_crop = frame.copy()

            # ROI_MARGIN, ROI_BOX 2개 생성
            roi_rect = ROI
            laneDetectHandler.set_roi(roi_rect)

            roi_box = {
                "x": roi_rect[0],
                "y": roi_rect[1],
                "w": roi_rect[2] - roi_rect[0],
                "h": roi_rect[3] - roi_rect[1],
            }
            roi_margin_box = {
                "x": roi_rect[0] - 100,
                "y": roi_rect[1] - 100,
                "w": roi_rect[2] - roi_rect[0] + 200,
                "h": roi_rect[3] - roi_rect[1] + 200,
            }

            frame_roi = image_util.crop_image(frame, roi_margin_box)
            #frame_roi = frame


        else:
            print('Video has ended or failed, try a different video format!')
            break
        print('Frame #: ', frame_num)

        if LaneDetectHandler.on_frame_valid(frame_num) == False:
            continue

        # 2.2 YOLO image setting, and get bbox, score, class
        real_frame_size = frame.shape[:2]
        frame_size = frame_roi.shape[:2]
        image_data = cv2.resize(frame_roi, (input_size, input_size))
        image_data = image_data / 255.
        image_data = image_data[np.newaxis, ...].astype(np.float32)
        start_time = time.time()
 

        # YOLO V4 inference
        batch_data = tf.constant(image_data)
        pred_bbox = infer(batch_data)
        for key, value in pred_bbox.items():
            boxes = value[:, :, 0:4]
            pred_conf = value[:, :, 4:]

        
        # get result by combined_non_max_suppression
        boxes, scores, classes, valid_detections = tf.image.combined_non_max_suppression(
            boxes=tf.reshape(boxes, (tf.shape(boxes)[0], -1, 1, 4)),
            scores=tf.reshape(
                pred_conf, (tf.shape(pred_conf)[0], -1, tf.shape(pred_conf)[-1])),
            max_output_size_per_class=50,
            max_total_size=50,
            iou_threshold=IOU,
            score_threshold=SCORE
        )
        

        # convert data to numpy arrays and slice out unused elements
        num_objects,bboxes,scores,classes = drh.convert_data_to_array_and_slice(valid_detections,boxes,scores,classes)


        # format bounding boxes from normalized ymin, xmin, ymax, xmax ---> xmin, ymin, width, height
        original_h, original_w, _ = frame_roi.shape
        bboxes = utils.format_boxes(bboxes, original_h, original_w)
        bboxes = common_util.boxes_add_roi(bboxes, roi_margin_box)

        # store all predictions in one parameter for simplicity when calling functions
        pred_bbox = [bboxes, scores, classes, num_objects]

        # read in all class names from config
        class_names = utils.read_class_names(cfg.YOLO.CLASSES)

        # 2.3 custom allowed classes (uncomment line below to customize tracker for only people)
        allowed_classes = ['car', 'truck','bus']


        # loop through objects and use class index to get class name, allow only classes in allowed_classes list
        names = []
        deleted_indx = []
        names, deleted_indx, count = drh.check_allow_class(allowed_classes,class_names,classes,names,deleted_indx,num_objects,bboxes)

        # delete detections that are not in allowed_classes
        bboxes = np.delete(bboxes, deleted_indx, axis=0)
        scores = np.delete(scores, deleted_indx, axis=0)

        # encode yolo detections and feed to tracker
        if laneDetectHandler.is_start():
            cv2.putText(frame, 'Lane detecting',((roi_rect[0]), (roi_rect[1])), cv2.FONT_HERSHEY_SIMPLEX, 2, (255, 255, 255), 2)
            first_lane_images = []
            other_lane_images = []
            features = encoder(frame, bboxes)
            
            result_obj_list = drh.get_Deep_SORT_result(features,roi_box,frame,real_frame_size,bboxes,scores, names ,tracker)
            
            result = laneDetectHandler.on_frame_result(frame_num, frame, result_obj_list)
            if result:
                # 차선 인식 완료
                print("차선 인식 완료")

        else:
            cv2.putText(frame, 'illegal_car detecting',((roi_rect[0]), (roi_rect[1])), cv2.FONT_HERSHEY_SIMPLEX, 2, (255, 255, 255), 2)
            first_lane_images = []
            first_lane_bboxes = []
            other_lane_images = []
            other_lane_bboxes = []
            crop_image = None
            result_obj_list = drh.get_only_YOLO_result(roi_box,frame,real_frame_size,bboxes,names)
            
            line_points = laneDetectHandler.predict(frame, result_obj_list)
            cv2.line(frame, line_points[0], line_points[1], (100,100,255), thickness=5)
            print('1차선 차들 검색중')
            
            
            # 1차선 차량이 아닌경우 images리스트에서 제거
            for rol in result_obj_list:
                if rol['is_on_first_lane'] ==True:
                    first_lane_images.append(rol['image'])
                    first_lane_bboxes.append(rol['box'])
                else:
                    other_lane_images.append(rol['image'])
                    other_lane_bboxes.append(rol['box'])


            print(f'1차선에 발견된 차 NUM: {len(first_lane_images)}')


        # 1차선에 차들이 다닐 때
        if len(first_lane_images) != 0:
            pred_list = helper.predict_batch(first_lane_images) 
            
            for i, pred in enumerate(pred_list):
                # 한글 폰트 깨지는거 해결 & 입력할 한글 text 만들기
                c_box = first_lane_bboxes[i] 
                
                red_color = (255,0,0)
                sky_color = (150,150,255)
                for item in pred:
                    class_id = item
                    kor_car_name = class_dict[str(class_id)]
                    eng_car_name = CLASS_CONV_DICT[kor_car_name]
                    title = eng_car_name

                # img_pil = Image.fromarray(frame)
                # draw = ImageDraw.Draw(img_pil)

                # 1차선에 다니는 차가 위법 차량일 때
                if kor_car_name in illegal_car_list:
                    cv2.putText(frame, title,(int(c_box["x"]), int(c_box["y"])), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)
                    #draw.text((int(c_box["x"]), int(c_box["y"]-28)), title, font=font, fill = (b,g,r,a))q
                    #frame = np.array(img_pil)
                    cv2.rectangle(frame, (int(c_box["x"]), int(c_box["y"])), (int(c_box["w"])+int(c_box["x"]), int(c_box["h"]) + int(c_box["y"])), red_color, 2)
                    
                    # 4. SideView 표시
                    if side_view_handler.side_frame_valid(frame_num) == True:
                        crop_image = image_util.crop_image(frame_to_crop, c_box)
                    side_view_handler.display_sideview(frame, crop_image) 
                
                # 1차선에 다니는 차가 합법 차량일 때
                else:
                    cv2.putText(frame, title,(int(c_box["x"]), int(c_box["y"])), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)
                    #draw.text((int(c_box["x"]), int(c_box["y"]-28)), title, font=font, fill = (b,g,r,a))
                    #frame = np.array(img_pil)
                    cv2.rectangle(frame, (int(c_box["x"]), int(c_box["y"])), (int(c_box["w"])+int(c_box["x"]), int(c_box["h"]) + int(c_box["y"])), sky_color, 2)      
                    side_view_handler.display_sideview(frame, crop_img = None) 
                                       
        # 1차선에 차가 없을 때    
        else:
            side_view_handler.display_sideview(frame, crop_img = None)
        
        # 1차선 제외 차 그리기 (디버깅을 위해 놔둠)
        # if len(other_lane_images) != 0:
        #     for obox in other_lane_bboxes:

        #         blue_color = (0,255,0)
        #         c_box = obox
        #         title = "other_lane_car"
        #         img_pil = Image.fromarray(frame)
        #         draw = ImageDraw.Draw(img_pil)

        #         draw.text((int(c_box["x"]), int(c_box["y"]-28)), title, font=font, fill = (b,g,r,a))
        #         frame = np.array(img_pil)
        #         cv2.rectangle(frame, (int(c_box["x"]), int(c_box["y"])), (int(c_box["w"])+int(c_box["x"]), int(c_box["h"]) + int(c_box["y"])), blue_color, 2)


            
            # cv2.rectangle(frame, (int(bbox[0]), int(bbox[1]-30)), (int(bbox[0])+(len(class_name)+len(str(track.track_id)))*17, int(bbox[1])), color, -1)

            # 한글 깨지던 코드
            #cv2.putText(frame, title, (int(bbox[0]), int(bbox[1]-10)),0, 0.75, (255,255,255),2)


        # draw ROI
        cv2.rectangle(frame, (roi_rect[0], roi_rect[1]), (roi_rect[2], roi_rect[3]), (255, 255, 255), 2)

        # draw MARGIN ROI
        #cv2.rectangle(frame, (roi_margin_box["x"], roi_margin_box["y"]), (roi_margin_box["w"]+roi_margin_box['x'], roi_margin_box["h"]+roi_margin_box['y']), (150, 150, 150), 2)

        # draw Line (디버그 용)
        # stnd_line_points = ((roi_rect[2]-200, roi_rect[3]),(roi_rect[2], roi_rect[3]))
        # cv2.line(frame, stnd_line_points[0], stnd_line_points[1], (0,0,255), thickness=5)

        # calculate frames per second of running detections
        fps = 1.0 / (time.time() - start_time)
        print("FPS: %.2f" % fps)
        result = np.asarray(frame)
        result = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
        
        cv2.imshow("Output Video", result)

        # r 버튼 누르면 
        if cv2.waitKey(1)  == ord('r'):
            print('r 버튼누름')
            laneDetectHandler.start(force_retrain=True)

        # q 누르면 종료
        if cv2.waitKey(1) == ord('q'):
            break

        # c 버튼 누르면 capture
        if cv2.waitKey(1) == ord('c'):
            cv2.imwrite("dron_car_detect.jpg", result)

    cv2.destroyAllWindows()
    print('종료')

if __name__ == '__main__':
    try:
        app.run(main)
    except SystemExit:
        pass
