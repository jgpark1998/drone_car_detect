# FLAG settings
WEIGHTS = './checkpoints/yolov4-416'
SIZE =  416
#ROI = (500, 300, 1300, 800)   # 1번영상
ROI = (400, 250, 1300, 800)    # 2번 영상
IOU = 0.45
SCORE =  0.50

# SideView parameter
SIDEVIEW_NUM = 7



#  illegal_car_settings
illegal_car_list=[ 
    "승합",
    "트럭_대형",
    "트럭_소형",
    "SUV"
]
CLASS_CONV_DICT = {
    "SUV": "SUV",
    "버스_대형": "Bus-large",
    "버스_중형": "Bus-medium",
    "세단": "Sedan",
    "승합": "Van",
    "트럭_대형": "Truck-big",
    "트럭_소형": "Truck-small",
}


# get LANE settings
SORT_THRESHOLD = 100
TEST_FRAME_NUM = 20
CLUSTERING_THRESHOLD = 5

# handler settings
FRAME_UNIT = 1
MOVING_THRESHOLD = 5
TRACE_DRAWING = True

# initialize LANE
LANE_REFRESH = True