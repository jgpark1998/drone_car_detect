import os
# comment out below line to enable tensorflow logging outputs
# os.environ["CUDA_VISIBLE_DEVICES"] = "0"
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import time
import tensorflow as tf
physical_devices = tf.config.experimental.list_physical_devices('GPU')
if len(physical_devices) > 0:
    tf.config.experimental.set_memory_growth(physical_devices[0], True)
    tf.config.experimental.set_visible_devices(physical_devices[0], 'GPU')
from absl import app, flags
from absl.flags import FLAGS
import core.utils as utils
from core.yolov4 import filter_boxes
from tensorflow.python.saved_model import tag_constants
from core.config import cfg
from PIL import Image, ImageFont, ImageDraw
import cv2
import numpy as np
import matplotlib.pyplot as plt
from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession


# from action_class_helper import ActionClassHelper

from not_use.opencv_ffmpeg import FFmpegVideoCapture

import drone_util.common_util as common_util
import drone_util.image_util as image_util
import drone_util.dron_result_handler2 as drh
from model_data.car_type import settings_car_type as settings 
from drone_util.efnet_helper_ver2 import EFNetHelper
from drone_util.lane_detect_handler_optical import LaneDetectHandler

# FLAG settings
flags.DEFINE_string('framework', 'tf', '(tf, tflite, trt')
flags.DEFINE_string('weights', './checkpoints/yolov4-608','path to weights file')
flags.DEFINE_integer('size', 416, 'resize images to')
flags.DEFINE_boolean('tiny', False, 'yolo or yolo-tiny')
flags.DEFINE_string('model', 'yolov4', 'yolov3 or yolov4')
flags.DEFINE_string('video', './data/video/test.mp4', 'path to input video or set to 0 for webcam')
flags.DEFINE_string('output', None, 'path to output video')
flags.DEFINE_string('output_format', 'MP4V', 'codec used in VideoWriter when saving video to file')
flags.DEFINE_float('iou', 0.45, 'iou threshold')
flags.DEFINE_float('score', 0.50, 'score threshold')
flags.DEFINE_boolean('dont_show', False, 'dont show video output')
flags.DEFINE_boolean('info', False, 'show detailed info of tracked objects')
flags.DEFINE_boolean('count', False, 'count objects being tracked on screen')
flags.DEFINE_integer('stop_frame_num', 1000000, 'frame that how much you want to see, it must to divided by 3')
flags.DEFINE_integer('skip_frame_num',0,'frames that not want to see' )

illegal_car_list=[ 
    "승합",
    "트럭_대형",
    "트럭_소형",
    "SUV"
]
CLASS_CONV_DICT = {
    "SUV": "SUV",
    "버스_대형": "Bus-large",
    "버스_중형": "Bus-medium",
    "세단": "Sedan",
    "승합": "Van",
    "트럭_대형": "Truck-big",
    "트럭_소형": "Truck-small",
}

# 실행코드
#python dron_car_detect2.py --video "./data/video/DRON_VIDEO.mp4" --output ./outputs/DRON_VIDEO.mp4 --model yolov4 --weights "./checkpoints/yolov4-416" --size 416  --stop_frame_num 10000 --skip_frame_num 0


# ROI_DEFAULT = (400,450, 1200, 800)    # 드 론 영 상
# ROI_DEFAULT = (600, 450, 1400, 900)
ROI_DEFAULT = (500, 400, 1500, 900)
ROI_DICT = {
    "20211010-120955-1633835395.mp4": (120, 120, 1800, 1000),
    "20211018-095956-1634518796.mp4": (120, 100, 1900, 1000)
}

# 차선 학습 객체 생성
laneDetectHandler = LaneDetectHandler(model_path="lane_detect.json", sort_threshold=200, test_frame_num=20, clustering_threshold=5)
laneDetectHandler.start()

# 폰트, 글자크기, 설정
b,g,r,a = 255,255,255,0            
fontpath = "NanumSquareB.ttf"
font = ImageFont.truetype(fontpath,30)

# EFNet Helper 객체 선언
helper = EFNetHelper(settings)

def main(_argv):

    # 1. SORT 객체 생성, YOLO 설정, Video Capture 설정
    class_dict = common_util.load_json(settings.CLASS_TEXT_PATH)

    # load configuration for object detector
    config = ConfigProto()
    config.gpu_options.allow_growth = True
    session = InteractiveSession(config=config)
    STRIDES, ANCHORS, NUM_CLASS, XYSCALE = utils.load_config(FLAGS)
    input_size = FLAGS.size
    video_path = FLAGS.video


    # 1-2. load tflite model if flag is set
    if FLAGS.framework == 'tflite':
        interpreter = tf.lite.Interpreter(model_path=FLAGS.weights)
        interpreter.allocate_tensors()
        input_details = interpreter.get_input_details()
        output_details = interpreter.get_output_details()
        print(input_details)
        print(output_details)
    # otherwise load standard tensorflow saved model
    else:
        saved_model_loaded = tf.saved_model.load(FLAGS.weights, tags=[tag_constants.SERVING])
        infer = saved_model_loaded.signatures['serving_default']


    # 1-3. setting VideoCapture
    # vid = cv2.VideoCapture(video_path)
    # width = int(vid.get(cv2.CAP_PROP_FRAME_WIDTH))
    # height = int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT))



    # 캡처카드 사용시
    vid = cv2.VideoCapture(0)
    width = 1920
    height = 1080

    #vid_ffmpeg = FFmpegVideoCapture(video_path, width, height, "bgr24")
    
    out = None

    # 1-4. get video ready to save locally if flag is set
    if FLAGS.output:
        # by default VideoCapture returns float instead of int
        width = int(vid.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT))
        # fps = int(vid.get(cv2.CAP_PROP_FPS))
        fps = 10
        codec = cv2.VideoWriter_fourcc(*'DIVX') # FLAGS.output_format
        out = cv2.VideoWriter(FLAGS.output, codec, fps, (width, height))
    
    frame_num = -1
    
    # 2. 매 frame 마다 YOLO, SORT, 실행 후 시각화 
    # while video is running
    while True:
        w_start = time.time()
        temp_dict = {}
        # 2.1 read video, apply masking, apply ROI
        frame_num +=1
        return_value, frame = vid.read()
        if return_value:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

            #Image MASK 적용
            #masked_frame = image_util.get_masked_image(frame)

            # ROI_MARGIN, ROI_BOX 2개 생성
            video_file = os.path.basename(video_path)
            roi_rect = ROI_DICT.get(video_file, ROI_DEFAULT)
            laneDetectHandler.set_roi(roi_rect)

            roi_box = {
                "x": roi_rect[0],
                "y": roi_rect[1],
                "w": roi_rect[2] - roi_rect[0],
                "h": roi_rect[3] - roi_rect[1],
            }
            roi_margin_box = {
                "x": roi_rect[0]-100,
                "y": roi_rect[1]-100,
                "w": roi_rect[2] - roi_rect[0] + 200,
                "h": roi_rect[3] - roi_rect[1] + 200,
            }

            frame_roi = image_util.crop_image(frame, roi_margin_box)
            #frame_roi = frame


        else:
            print('Video has ended or failed, try a different video format!')
            break
        print('Frame #: ', frame_num)

        # stop_frame_num 만큼 inference를 실행합니다
        if frame_num >= FLAGS.stop_frame_num:
            break

        if LaneDetectHandler.on_frame_valid(frame_num) == False:
            continue


        # # 3의 배수의 frame_num 일 때만 inference
        # if frame_num % 3 != 0:
        #     #frame_num += 1   
        #     continue
 
        # skip_frame_num 만큼 inference를 skip 합니다
        if frame_num < FLAGS.skip_frame_num:
            frame_num += 1
            continue

        # if frame_num >= 30 * 60:   # 1분까지만 유효
        #     break

        # 2.2 YOLO image setting, and get bbox, score, class
        real_frame_size = frame.shape[:2]
        frame_size = frame_roi.shape[:2]
        image_data = cv2.resize(frame_roi, (input_size, input_size))
        image_data = image_data / 255.
        image_data = image_data[np.newaxis, ...].astype(np.float32)
        start_time = time.time()
        a_time = time.time()

        # run detections on tflite if flag is set
        if FLAGS.framework == 'tflite':
            interpreter.set_tensor(input_details[0]['index'], image_data)
            interpreter.invoke()
            pred = [interpreter.get_tensor(output_details[i]['index']) for i in range(len(output_details))]
            # run detections using yolov3 if flag is set
            if FLAGS.model == 'yolov3' and FLAGS.tiny == True:
                boxes, pred_conf = filter_boxes(pred[1], pred[0], score_threshold=0.25,
                                                input_shape=tf.constant([input_size, input_size]))
            else:
                boxes, pred_conf = filter_boxes(pred[0], pred[1], score_threshold=0.25,
                                                input_shape=tf.constant([input_size, input_size]))
        else:
            # YOLO V4 inference
            batch_data = tf.constant(image_data)
            pred_bbox = infer(batch_data)
            for key, value in pred_bbox.items():
                boxes = value[:, :, 0:4]
                pred_conf = value[:, :, 4:]

        
        # get result by combined_non_max_suppression
        boxes, scores, classes, valid_detections = tf.image.combined_non_max_suppression(
            boxes=tf.reshape(boxes, (tf.shape(boxes)[0], -1, 1, 4)),
            scores=tf.reshape(
                pred_conf, (tf.shape(pred_conf)[0], -1, tf.shape(pred_conf)[-1])),
            max_output_size_per_class=50,
            max_total_size=50,
            iou_threshold=FLAGS.iou,
            score_threshold=FLAGS.score
        )
        

        # convert data to numpy arrays and slice out unused elements
        num_objects,bboxes,scores,classes = drh.convert_data_to_array_and_slice(valid_detections,boxes,scores,classes)


        # format bounding boxes from normalized ymin, xmin, ymax, xmax ---> xmin, ymin, width, height


        original_h, original_w, _ = frame_roi.shape
        bboxes = utils.format_boxes(bboxes, original_h, original_w)

        bboxes = common_util.boxes_add_roi(bboxes, roi_margin_box)

        # store all predictions in one parameter for simplicity when calling functions
        pred_bbox = [bboxes, scores, classes, num_objects]

        # read in all class names from config
        class_names = utils.read_class_names(cfg.YOLO.CLASSES)

        # 2.3 custom allowed classes (uncomment line below to customize tracker for only people)
        # allowed_classes = ['car', 'bus', 'truck']
        allowed_classes = ['car', 'truck','bus']


        # loop through objects and use class index to get class name, allow only classes in allowed_classes list
        names = []
        deleted_indx = []
        names, deleted_indx, count = drh.check_allow_class(allowed_classes,class_names,classes,names,deleted_indx,num_objects,bboxes)

        if FLAGS.count:
            cv2.putText(frame, "Objects being tracked: {}".format(count), (5, 35), cv2.FONT_HERSHEY_COMPLEX_SMALL, 2, (0, 255, 0), 2)
            print("Objects being tracked: {}".format(count))

        # delete detections that are not in allowed_classes
        bboxes = np.delete(bboxes, deleted_indx, axis=0)
        scores = np.delete(scores, deleted_indx, axis=0)

        # encode yolo detections and feed to tracker

        #initialize color map
        cmap = plt.get_cmap('tab20b')
        #colors = [cmap(i)[:3] for i in np.linspace(0, 1, 20)]
        print(f'YOLO 감지까지 걸린 시간:{time.time()-a_time}')
        if laneDetectHandler.is_start():
            first_lane_images = []
            other_lane_images = []
            
            result_obj_list = []
            
            result = laneDetectHandler.on_frame_result(frame_num, frame, result_obj_list)
            if result:
                # 차선 인식 완료
                print("차선 인식 완료")

        else:
         
            first_lane_images = []
            first_lane_bboxes = []
            other_lane_images = []
            other_lane_bboxes = []
            # YOLO만 돌리는 경우 작성해야함
            result_obj_list = drh.get_only_YOLO_result(roi_box,frame,real_frame_size,bboxes,names)

            # # 1차선을 계속 그려야함-> predict에서 함
            
            line_points = laneDetectHandler.predict(frame, result_obj_list)

            
            # -> 위 함수 호출로 기존 car detection 결과에 is_on_first_lane 이라는 key 생성되었을 것
            print('1차선 차들 검색중')
            cv2.line(frame, line_points[0], line_points[1], (0,0,255), thickness=5)
        
            # TODO: efnet으로 Predict 인식된 차선으로 1차선 위의 차량 판단 나머지 흰색, 1차선 파란색 위반 빨간색
            
            # 1차선 차량이 아닌경우 images리스트에서 제거
            for rol in result_obj_list:
                if rol['is_on_first_lane'] ==True:
                    first_lane_images.append(rol['image'])
                    first_lane_bboxes.append(rol['box'])
                else:
                    other_lane_images.append(rol['image'])
                    other_lane_bboxes.append(rol['box'])


            print(f'1차선에 발견된 차 NUM: {len(first_lane_images)}')


        # inference
        
        if len(first_lane_images) != 0:
            b_time = time.time()
            pred_list = helper.predict_batch(first_lane_images)    
            print(f'EFNET infer TIME:{time.time()-b_time}')
            
            draw_time = time.time()
            for i, pred in enumerate(pred_list):
                # 한글 폰트 깨지는거 해결 & 입력할 한글 text 만들기
                c_box = first_lane_bboxes[i] 
                
                red_color = (255,0,0)
                sky_color = (150,150,255)
                for item in pred:
                    class_id = item
                    kor_car_name = class_dict[str(class_id)]
                    eng_car_name = CLASS_CONV_DICT[kor_car_name]
                    title = eng_car_name

                # img_pil = Image.fromarray(frame)
                # draw = ImageDraw.Draw(img_pil)
                if kor_car_name in illegal_car_list:
                    cv2.putText(frame, title,(int(c_box["x"]), int(c_box["y"])), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)
                    #draw.text((int(c_box["x"]), int(c_box["y"]-28)), title, font=font, fill = (b,g,r,a))
                    #frame = np.array(img_pil)
                    cv2.rectangle(frame, (int(c_box["x"]), int(c_box["y"])), (int(c_box["w"])+int(c_box["x"]), int(c_box["h"]) + int(c_box["y"])), red_color, 2)
                else:
                    cv2.putText(frame, title,(int(c_box["x"]), int(c_box["y"])), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)
                    #draw.text((int(c_box["x"]), int(c_box["y"]-28)), title, font=font, fill = (b,g,r,a))
                    #frame = np.array(img_pil)
                    cv2.rectangle(frame, (int(c_box["x"]), int(c_box["y"])), (int(c_box["w"])+int(c_box["x"]), int(c_box["h"]) + int(c_box["y"])), sky_color, 2)                    
        # TODO 2,3차선 차 그리기
        # if len(other_lane_images) != 0:
        #     for obox in other_lane_bboxes:

        #         blue_color = (0,255,0)
        #         c_box = obox
        #         title = "other_lane_car"
        #         img_pil = Image.fromarray(frame)
        #         draw = ImageDraw.Draw(img_pil)

        #         draw.text((int(c_box["x"]), int(c_box["y"]-28)), title, font=font, fill = (b,g,r,a))
        #         frame = np.array(img_pil)
        #         cv2.rectangle(frame, (int(c_box["x"]), int(c_box["y"])), (int(c_box["w"])+int(c_box["x"]), int(c_box["h"]) + int(c_box["y"])), blue_color, 2)
            print(f'모든 차들 그리는데 까지 걸린시간 TIME:{time.time()-draw_time}')


            
            # cv2.rectangle(frame, (int(bbox[0]), int(bbox[1]-30)), (int(bbox[0])+(len(class_name)+len(str(track.track_id)))*17, int(bbox[1])), color, -1)

            # 한글 깨지던 코드
            #cv2.putText(frame, title, (int(bbox[0]), int(bbox[1]-10)),0, 0.75, (255,255,255),2)

        
        # draw ROI
        cv2.rectangle(frame, (roi_rect[0], roi_rect[1]), (roi_rect[2], roi_rect[3]), (255, 255, 255), 2)

        # draw MARGIN ROI
        #cv2.rectangle(frame, (roi_margin_box["x"], roi_margin_box["y"]), (roi_margin_box["w"]+roi_margin_box['x'], roi_margin_box["h"]+roi_margin_box['y']), (150, 150, 150), 2)

        # draw Line
        stnd_line_points = ((roi_rect[2]-200, roi_rect[3]),(roi_rect[2], roi_rect[3]))
        cv2.line(frame, stnd_line_points[0], stnd_line_points[1], (0,0,255), thickness=5)



        # calculate frames per second of running detections
        print(f'full_time: {time.time() - w_start}')
        fps = 1.0 / (time.time() - start_time)
        print("FPS: %.2f" % fps)
        result = np.asarray(frame)
        result = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
        
        if not FLAGS.dont_show:
            #result = cv2.resize(result, (800,700))
            cv2.imshow("Output Video", result)
            
        # if output flag is set, save video file
        # if FLAGS.output:
        #     out.write(result)
        #     cv2.imwrite(f"outputs/frame/{frame_num}.jpg", result)

        # if output flag is set, save video file
        if FLAGS.output:
            out.write(result)
            #cv2.imwrite("dron_car_detect.jpg", result)


        # r 버튼 누르면 
        if cv2.waitKey(1)  == ord('r'):
            print('r 버튼누름')
            laneDetectHandler.start(force_retrain=True)

        # q 누르면 종료
        if cv2.waitKey(1) == ord('q'):
            break
    cv2.destroyAllWindows()

if __name__ == '__main__':
    try:
        app.run(main)
    except SystemExit:
        pass
